﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisMovement : MonoBehaviour
{
    int speed = 10;
    int rotSpeed = 3;

    void FixedUpdate()
    {
        if (Input.GetButton("Horizontal"))
        {
            transform.position += Input.GetAxis("Horizontal") * transform.right * Time.deltaTime * speed;
        }

        if (Input.GetButton("Vertical"))
        {
            transform.position += Input.GetAxis("Vertical") * transform.forward * Time.deltaTime * speed;
        }

        if (Input.GetButton("Rotation"))
        {
            transform.Rotate(0, Input.GetAxis("Rotation") * rotSpeed, 0);
        }
    }
}
