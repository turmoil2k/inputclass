﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    float speed = 10;

    void FixedUpdate()
    {
        //MOVEMENT
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= transform.right * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * speed * Time.deltaTime;
        }

        //ROTATIONS
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(0, -1.5f, 0);
        }

        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(0, 1.5f, 0);
        }
    }
}
