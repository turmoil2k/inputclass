﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBMovement : MonoBehaviour
{
    Rigidbody rigidBody;
    Vector3 _EulerAngleVelocity;
    int speed = 20;

    void Start()
    {
        _EulerAngleVelocity = new Vector3(0,  100, 0);
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    void FixedUpdate()
    {
        if (Input.GetButton("Horizontal"))
        {
            rigidBody.velocity = Input.GetAxis("Horizontal") * transform.right * speed;
            //rigidBody.transform.position += Input.GetAxis("Horizontal") * Vector3.right * speed * Time.deltaTime;
        }

        if (Input.GetButton("Vertical"))
        {
            rigidBody.velocity = Input.GetAxis("Vertical") * transform.forward * speed;
            //rigidBody.transform.position += Input.GetAxis("Vertical") * Vector3.forward * speed * Time.deltaTime;
        }

        if (Input.GetButton("Rotation"))
        {
            Quaternion deltaRotation = Quaternion.Euler(_EulerAngleVelocity * Time.deltaTime * Input.GetAxis("Rotation"));
            rigidBody.MoveRotation(rigidBody.rotation * deltaRotation);
        }
    }
}
